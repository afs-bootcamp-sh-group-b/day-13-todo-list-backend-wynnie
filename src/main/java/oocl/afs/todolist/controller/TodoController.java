package oocl.afs.todolist.controller;

import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    List<TodoResponse> getAll() {
        return todoService.findAll();
    }

    @PostMapping
    void create(@RequestBody TodoCreateRequest todoRequest){
        todoService.createItem(todoRequest);
    }

    @PutMapping("/{todoId}")
    TodoResponse update(@RequestBody TodoCreateRequest todoRequest, @PathVariable Long todoId){
        return todoService.updateItem(todoRequest,todoId);
    }

    @DeleteMapping("/{todoId}")
    void delete(@PathVariable Long todoId){
        todoService.deleteItem(todoId);
    }

}
