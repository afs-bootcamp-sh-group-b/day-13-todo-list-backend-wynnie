package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll()
                .stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    public void createItem(TodoCreateRequest request) {
        Todo todoAdd = TodoMapper.toEntity(request);
        todoRepository.save(todoAdd);
    }

    public TodoResponse updateItem(TodoCreateRequest todoUpdate, Long todoId) {
        Todo todoItem = todoRepository.findById(todoId).orElse(null);
        if (todoItem!=null){
            todoItem.setDone(todoUpdate.getDone());
            todoItem.setText(todoUpdate.getText());
            Todo todo = todoRepository.save(todoItem);
            return TodoMapper.toResponse(todo);
        }
        return null;
    }


    public void deleteItem(Long todoId) {
        todoRepository.deleteById(todoId);
    }
}
