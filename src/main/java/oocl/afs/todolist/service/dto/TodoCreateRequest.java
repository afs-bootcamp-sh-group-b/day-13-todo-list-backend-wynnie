package oocl.afs.todolist.service.dto;

public class TodoCreateRequest {
    private String text;
    private Boolean done;

    public TodoCreateRequest() {
        this.done = false;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
