package oocl.afs.todolist;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        todoRepository.save(todo);

        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()));
    }
    @Test
    void should_add_todo_item() throws Exception {
        String json = "{\"text\": \"spring\"}";
        mockMvc.perform(post("/todo").content(json).contentType("application/json"))
                .andExpect(status().isOk());
        Long id = todoRepository.findAll().get(0).getId();
        final boolean success = todoRepository.findById(id).stream()
                .allMatch(todoItem -> todoItem.getText().equals("spring"));
        assertTrue(success);
    }

    @Test
    void should_update_todo_done() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        todoRepository.save(todo);
        Long id = todoRepository.findAll().get(0).getId();
        String json = "{\"text\": \"Study React\" , \"done\": true}";
        mockMvc.perform(put("/todo/"+id).content(json).contentType("application/json"))
                .andExpect(status().isOk())
                .andExpectAll(MockMvcResultMatchers.jsonPath("$.id").isNumber(),
                        MockMvcResultMatchers.jsonPath("$.text").value("Study React"),
                        MockMvcResultMatchers.jsonPath("$.done").value(true));
    }
    @Test
    void should_update_todo_text() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        todoRepository.save(todo);
        Long id = todoRepository.findAll().get(0).getId();
        String json = "{\"text\": \"spring\" , \"done\": false}";
        mockMvc.perform(put("/todo/"+id).content(json).contentType("application/json"))
                .andExpect(status().isOk())
                .andExpectAll(MockMvcResultMatchers.jsonPath("$.id").isNumber(),
                        MockMvcResultMatchers.jsonPath("$.text").value("spring"),
                        MockMvcResultMatchers.jsonPath("$.done").value(false));
    }
    @Test
    void should_delete_todo_item() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        todoRepository.save(todo);
        Long id = todoRepository.findAll().get(0).getId();
        mockMvc.perform(delete("/todo/"+id))
                .andExpect(status().isOk());
        final boolean success = todoRepository.findById(id).isEmpty();
        assertTrue(success);
    }
}
